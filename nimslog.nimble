# Package

version       = "1.0.0"
author        = "Wazubaba"
description   = "Wrapper for the slog logging library"
license       = "MIT"
srcDir        = "src"

# Dependencies

requires "nim >= 0.18.1"
requires "nake >= 1.9.2"

