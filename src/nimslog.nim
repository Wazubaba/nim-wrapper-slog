{.compile:"slog/src/slog.c".}
##  
##  Define macros to allow us get further informations 
##  on the corresponding erros. These macros used as wrappers 
##  for slog() function.
## 
##  Definitions for version informations.

const
  SLOGVERSION_MAJOR* = 1
  SLOGVERSION_MINOR* = 5
  SLOGBUILD_NUM* = 1

##  If compiled on DARWIN/Apple platforms.

when defined(DARWIN):
  const
    CLOCK_REALTIME* = 0x2D4E1588
    CLOCK_MONOTONIC* = 0x00000000
##  Loging flags.

const
  SLOG_NONE* = 0
  SLOG_LIVE* = 1
  SLOG_INFO* = 2
  SLOG_WARN* = 3
  SLOG_DEBUG* = 4
  SLOG_ERROR* = 5
  SLOG_FATAL* = 6
  SLOG_PANIC* = 7

##  Supported colors.

const
  CLR_NORMAL* = "\e[0m"
  CLR_RED* = "\e[31m"
  CLR_GREEN* = "\e[32m"
  CLR_YELLOW* = "\e[33m"
  CLR_BLUE* = "\e[34m"
  CLR_NAGENTA* = "\e[35m"
  CLR_CYAN* = "\e[36m"
  CLR_WHITE* = "\e[37m"
  CLR_RESET* = "\e[0m"

type
  SlogFlags* {.bycopy.} = object
    fname*: cstring
    file_level*: cshort
    level*: cshort
    to_file*: cshort
    pretty*: cshort
    filestamp*: cshort
    td_safe*: cshort

  SlogDate* {.bycopy.} = object
    year*: cint
    mon*: cint
    day*: cint
    hour*: cint
    min*: cint
    sec*: cint
    usec*: cint


## 
##  FUNCTION: slog_version.
##  DESCRIPTION: Get slog library version.
##  PARAM: (min) is flag for output format.
##         If (min) is 1, function returns version in full format. (eg 1.4 build 85 (Jan 21 2017))
##         If (min) is 0 function returns only version numbers. (eg. 1.4.85)
##  RETURN: Version and build number of slog library.
## 

proc slog_version*(min: cint): cstring {.cdecl, importc:"slog_version".}
## 
##  FUNCTION: strclr.
##  DESCRIPTION: Colorize the given string.
##  PARAMS: (clr) is color value defined above. If it is invalid, function returns NULL.
##          (str) is string with va_list of arguments which one we want to colorize.
##  RETURN: The colorized string.
## 

proc strclr*(clr: cstring; str: cstring): cstring {.varargs, cdecl, importc:"strclr".}
## 
##  FUNCTION: slog_get.
##  DESCRIPTION: Create a slog formated string.
##  PARAMS: (pDate) holds the current date-time format.
##          (msg) is string that holds informations about the log action.
##  RETURN: Generating string in form:
##          yyyy.mm.dd-HH:MM:SS.UU - (some message)
## 

proc slog_get*(pDate: ptr SlogDate; msg: cstring): cstring {.varargs, cdecl, importc:"slog_get".}
## 
##  FUNCTION: slog.
##  DESCRIPTION: Log exiting process. We save log in file if LOGTOFILE flag
##               is enabled from config.
##  PARAMS: (level) logging level.
##          (flag) is slog flag defined above.
##          (msg) is the user defined message for the current log action.
##  RETURN: (void)
## 

proc slog*(level: cint; flag: cint; msg: cstring) {.varargs, cdecl, importc:"slog".}
## 
##  FUNCTION: slog_init.
##  DESCRIPTION: Function parses config file, reads log level and save
##  to file flag from config.
##  PARAMS: (fname) log file name where log informations will be saved.
##          (conf) config file path to be parsed. If is NULL the default values are set.
##          (lvl) log level. If you will not initialize slog, it will only
##          print messages with log level 0.
##          (flvl) same as above with the difference that corresponds to file level.
##          (t_safe) thread safety flag (1 enabled, 0 disabled).
##  RETURN: (void)
## 

proc slog_init*(fname: cstring; conf: cstring; lvl: cint; flvl: cint; t_safe: cint) {.cdecl, importc:"slog_init".}

