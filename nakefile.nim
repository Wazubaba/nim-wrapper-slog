import nake
import strformat
import os

const
  name = "nimslog"
  src = "src/nimslog.nim"
  rebuild_on = @[src]
  cache = ".nim"
  version = "1.0.0"

when defined(linux):
  const suffix_static = ".a"
  const suffix_dynamic = ".so"
  const prefix = "lib"

elif defined(windows):
  const suffix_static = ".lib"
  const suffix_dynamic = ".dll"
  const prefix = ""

elif defined(osx):
  const suffix_static = ".a"
  const suffix_dynamic = ".dynlib"
  const prefix = "lib"

else:
  echo "Platform is not supported by the build file, please build manually"
  quit 1

const
  bin_name = fmt"{prefix}{name}"

task "static", "Build a static library":
  if name.needsRefresh(rebuild_on):
    direShell(nimExe, "c", "--threads:on", "--app:staticlib", fmt"--out:{bin_name}{suffix_static}", fmt"--nimcache:{cache}", src)

task "dynamic", "Build as a dynamic library":
  if name.needsRefresh(rebuild_on):
     direShell(nimExe, "c", "--threads:on", "--app:lib", fmt"--out:{bin_name}{suffix_dynamic}", fmt"--nimcache:{cache}", src)   

task "all", "Build both the static and dynamic library":
  runTask("static")
  runTask("dynamic")

task "clean", "Clean all generated build-files":
  if cache.dirExists():
    removeDir(cache)

  for item in [suffix_static, suffix_dynamic]:
    if fmt"{bin_name}{item}".fileExists():
      removeFile(fmt"{bin_name}{item}")
  
task "dist-clean", "Clean everything. Yes":
  runTask("clean")

#  if "release".dirExists():
#    removeDir("release")

#  if "dist".dirExists():
#    removeDir("dist")

  if "nakefile".fileExists():
    removeFile("nakefile")

  if "nimcache".dirExists():
    removeDir("nimcache")

#task "package", "Build a release package":
#  runTask("dist-clean")
#  runTask("release")
#  createDir("release")
#  createDir("dist")
#  createDir(joinPath("release", "bin"))
#  createDir(joinPath("release", "etc"))
#  createDir(joinPath("release", "doc"))
#  copyFile(name, joinPath("release", "bin", name))
#  copyFile("gopherdrc", joinPath("release", "etc", "gopherdrc"))
#  copyFile("README.md", joinPath("release", "doc", "README.md"))
#  direShell("tar", "-c", "-z", "-f", joinPath("dist", fmt"{name}-{version}.tgz"), "release")
#  removeDir("release")
#  echo fmt"Created distribution package dist/{name}-{version}.tgz"

# Linux-specific tasks
#when defined(linux):
#  task "install", "Install program for system-wide use":
#    discard # TODO: Implement
